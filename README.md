# final_project

AOS 573 Final Project

The Jupyter notebook called final_project.ipynb creates multiple plots and tables of Ross Island Region Automatic Weather Station (AWS)
data that shows climatological data. This includes time series, monthly means, maximums and minimums.
The data used is from https://amrc.ssec.wisc.edu/data/ftp/pub/aws/q3h/ and the format of these data are text files.

The data files named wfdYYYYMMq3h.txt (YYYY for the year; eg. 2015 and MM for month; eg. 01) are the monthly, three hourly quality controlled 
Willie Field data. The other files: wdb_data.txt, fer_data.txt, and cbd_data.txt are the combined data files for the other AWS I have already put together
for ease of use and limiting the number of files in this reopsitory.

There is an environment.yml file that tells you what packages are needed for this script and the environment that is used to run the notebook.
